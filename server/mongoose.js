import mongoose from 'mongoose'

const MONGO_URI =
  'mongodb+srv://loicburnotte:F93pP5zrQDPKpGz6@cluster0.j5jtgqy.mongodb.net/?retryWrites=true&w=majority'
if (!MONGO_URI) {
  throw new Error('You must provide a MongoLab URI')
}

mongoose
  .connect(MONGO_URI)
  .then(() => {
    console.log('Connected to MongoDB')
  })
  .catch(error => {
    console.error('Error connecting to MongoDB', error)
  })
