import { gql } from '@apollo/client/index.js'

export const typeDefs = gql`
  type Company {
    id: String!,
    name: { type: String! },
    description: { type: String },
    users: { type: User[] },
  }

  type User{
    id: String!,
    firstName: { type: String! },
    age: { type: Int! },
    company: { type: Company },
  }

  type Query {
    allUsers: [User],
    allCompanies: [Company]
  }
`
