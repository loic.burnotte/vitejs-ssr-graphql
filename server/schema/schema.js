import { GraphQLInt, GraphQLSchema, GraphQLString, GraphQLList, GraphQLNonNull, GraphQLObjectType } from 'graphql'
import axios from 'axios'

const serverURL = 'http://localhost:3000' // process.env.VITE_REACT_APP_API_URL

const CompanyType = new GraphQLObjectType({
  name: 'Company',
  fields: () => ({
    id: { type: GraphQLString },
    name: { type: GraphQLString },
    description: { type: GraphQLString },
    users: {
      type: new GraphQLList(UserType),
      resolve(parentValue, args) {
        // TODO: JSON server cannot retrieve nested lists...
        return axios.get(`${serverURL}/companies/${parentValue.id}/users`).then(res => res.data)
      },
    },
  }),
})

const UserType = new GraphQLObjectType({
  name: 'User',
  fields: () => ({
    id: { type: GraphQLString },
    firstName: { type: GraphQLString },
    age: { type: GraphQLInt },
    company: {
      type: CompanyType,
      resolve(parentValue, args) {
        if (!parentValue?.companyId) return null
        return axios.get(`${serverURL}/companies/${parentValue.companyId}`).then(res => res.data)
      },
    },
  }),
})

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    user: {
      type: UserType,
      args: { id: { type: GraphQLString } },
      resolve(parentValue, args) {
        return axios.get(`${serverURL}/users/${args.id}`).then(res => res.data)
      },
    },
    users: {
      type: new GraphQLList(UserType),
      args: {},
      resolve(parentValue, args) {
        return axios.get(`${serverURL}/users`).then(res => res.data)
      },
    },
    company: {
      type: CompanyType,
      args: { id: { type: GraphQLString } },
      resolve(parentValue, args) {
        return axios.get(`${serverURL}/companies/${args.id}`).then(res => res.data)
      },
    },
    companies: {
      type: new GraphQLList(CompanyType),
      args: {},
      resolve(parentValue, args) {
        return axios.get(`${serverURL}/companies`).then(res => res.data)
      },
    },
  },
})

const mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    /* USER */
    addUser: {
      type: UserType,
      args: {
        firstName: { type: new GraphQLNonNull(GraphQLString) },
        age: { type: new GraphQLNonNull(GraphQLInt) },
        companyId: { type: GraphQLString },
      },
      resolve(parentValue, { firstName, age, companyId }) {
        return axios.post(`${serverURL}/users`, { firstName, age, companyId }).then(res => res.data)
      },
    },
    editUser: {
      type: UserType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLString) },
        firstName: { type: GraphQLString },
        age: { type: GraphQLInt },
        companyId: { type: GraphQLString },
      },
      resolve(parentValue, args) {
        return axios.patch(`${serverURL}/users/${args.id}`, args).then(res => res.data)
      },
    },
    deleteUser: {
      type: UserType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve(parentValue, { id }) {
        return axios.delete(`${serverURL}/users/${id}`).then(res => res.data)
      },
    },
    /* COMPANY */
    addCompany: {
      type: CompanyType,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        description: { type: GraphQLString },
      },
      resolve(parentValue, { name, description }) {
        return axios.post(`${serverURL}/companies`, { name, description }).then(res => res.data)
      },
    },
    editCompany: {
      type: CompanyType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLString) },
        name: { type: new GraphQLNonNull(GraphQLString) },
        description: { type: GraphQLString },
      },
      resolve(parentValue, args) {
        return axios.patch(`${serverURL}/companies/${args.id}`, args).then(res => res.data)
      },
    },
    deleteCompany: {
      type: CompanyType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve(parentValue, { id }) {
        return axios.delete(`${serverURL}/companies/${id}`).then(res => res.data)
      },
    },
  },
})

export const schema = new GraphQLSchema({
  query: RootQuery,
  mutation,
})
