import js from '@eslint/js'

export default [
  js.configs.recommended,
  {
    files: ['**/*.{js,ts,tsx}'],
    // plugins: {
    //   '@typescript-eslint': eslint,
    // },
    // parser: '@typescript-eslint/parser',
    languageOptions: {
      node: true,
      ecmaVersion: 2022,
      sourceType: 'module',
      // parser,
    },
    ignores: ['node_modules/', 'dist/'],
    rules: {
      'no-console': 'warn',
      'no-unused-vars': 'off',
      // 'react-hooks/rules-of-hooks': 'error',
      // 'react-hooks/exhaustive-deps': 'warn',
      // '@typescript-eslint/no-unused-vars': ['warn', { argsIgnorePattern: '^_' }],
      // 'react-refresh/only-export-components': ['warn', { allowConstantExport: true }],
    },
  },
]
