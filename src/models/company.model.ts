import { UserType } from './user.model'

export type CompanyType = {
  id: string
  name: string
  description?: string
  users?: UserType[]
}
