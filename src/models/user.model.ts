import { CompanyType } from './company.model'

export type UserType = {
  id: string
  firstName: string
  age: number
  company?: CompanyType
}
