import { gql } from '@apollo/client/index.js'

const GET_COMPANIES = gql`
  query GetCompanies {
    companies {
      id
      name
      description
    }
  }
`

const GET_COMPANY = gql`
  query GetCompany($id: String!) {
    company(id: $id) {
      id
      name
      description
    }
  }
`

const ADD_COMPANY = gql`
  mutation addCompany($name: String!, $description: String) {
    addCompany(name: $name, description: $description) {
      id
      name
      description
    }
  }
`

const UPDATE_COMPANY = gql`
  mutation editCompany($id: String!, $name: String!, $description: String) {
    editCompany(id: $id, name: $name, description: $description) {
      id
      name
      description
    }
  }
`

const DELETE_COMPANY = gql`
  mutation deleteCompany($id: String!) {
    deleteCompany(id: $id) {
      id
    }
  }
`

export { GET_COMPANIES, GET_COMPANY, ADD_COMPANY, UPDATE_COMPANY, DELETE_COMPANY }
