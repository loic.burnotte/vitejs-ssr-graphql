import { gql } from '@apollo/client/index.js'

const GET_USERS = gql`
  query GetUsers {
    users {
      id
      firstName
      age
    }
  }
`

const GET_USER = gql`
  query GetUser($id: String!) {
    user(id: $id) {
      id
      firstName
      age
      company {
        id
        name
        description
      }
    }
  }
`

const ADD_USER = gql`
  mutation addUser($firstName: String!, $age: Int!, $companyId: String) {
    addUser(firstName: $firstName, age: $age, companyId: $companyId) {
      id
      firstName
      age
      company {
        name
      }
    }
  }
`

const UPDATE_USER = gql`
  mutation editUser($id: String!, $age: Int, $firstName: String, $companyId: String) {
    editUser(id: $id, age: $age, firstName: $firstName, companyId: $companyId) {
      id
      firstName
      age
      company {
        name
      }
    }
  }
`

const DELETE_USER = gql`
  mutation deleteUser($id: String!) {
    deleteUser(id: $id) {
      id
    }
  }
`

export { GET_USERS, GET_USER, ADD_USER, UPDATE_USER, DELETE_USER }
