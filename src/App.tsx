import Users from './components/Users'
import reactLogo from './assets/react.svg'
import Companies from './components/Companies'

import './App.css'

function App() {
  return (
    <div className="app">
      <a href="https://vitejs.dev" target="_blank">
        <img src="/vite.svg" className="logo" alt="Vite logo" />
      </a>
      <a href="https://reactjs.org" target="_blank">
        <img src={reactLogo} className="logo react" alt="React logo" />
      </a>
      <h1 className="mb-20 text-2xl">Vite + React + SSR + GraphQL</h1>
      <div className="flex flex-row justify-around w-full max-h-96 overflow-y-auto">
        <Users />
        <Companies />
      </div>
    </div>
  )
}

export default App
