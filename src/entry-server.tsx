import React from 'react'
import { StaticRouter } from 'react-router-dom/server'
import { type RenderToPipeableStreamOptions, renderToPipeableStream } from 'react-dom/server'
import Main from './main'

export function render(_url: string, _ssrManifest?: string, options?: RenderToPipeableStreamOptions) {
  return renderToPipeableStream(
    <React.StrictMode>
      <StaticRouter location="/">
        <Main />
      </StaticRouter>
    </React.StrictMode>,
    options
  )
}
