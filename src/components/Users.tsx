import { Link } from 'react-router-dom'
import { GET_USERS } from '../GraphQL/user'
import { useQuery } from '@apollo/client/index.js'
import type { UserType } from '../models/user.model'

function Users() {
  const { loading, error, data } = useQuery<{ users: UserType[] }>(GET_USERS)

  return (
    <div>
      <h1 className="text-4xl font-bold mb-4">Users</h1>
      {loading && 'Loading users...'}
      {error && `Error: ${error.message}`}
      {data?.users ? (
        <ul>
          {data.users?.map(user => (
            <Link key={user.id} to={`/user/${user.id}`}>
              <li>
                {user.firstName} ({user.age})
              </li>
            </Link>
          ))}
          <Link key="add" to="/users/add">
            <li className="rounded-3xl bg-cyan-500 text-white w-40 m-auto py-1 my-1">ADD USER</li>
          </Link>
        </ul>
      ) : (
        <h4>No data!</h4>
      )}
    </div>
  )
}

export default Users
