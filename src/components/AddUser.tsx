import Dropdown from './ui/dropdown'
import { useNavigate } from 'react-router-dom'
import { useCompanyStore } from '../store/company'
import { FormEvent, useRef, useState } from 'react'
import { useMutation } from '@apollo/client/index.js'
import { ADD_USER, GET_USERS } from '../GraphQL/user'

function User() {
  const redirect = useNavigate()
  const { companies } = useCompanyStore()

  const [companyId, setCompanyId] = useState<string>()
  const firstNameRef = useRef<HTMLInputElement>(null)
  const ageRef = useRef<HTMLInputElement>(null)

  const [addUser, addUserMutation] = useMutation(ADD_USER)

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const firstName = firstNameRef.current?.value
    const age = Number(ageRef.current?.value)

    await addUser({
      variables: {
        firstName,
        age,
        companyId,
      },
      refetchQueries: [GET_USERS],
    })

    if (addUserMutation.error) {
      console.error(addUserMutation.error.message)
      throw new Error(addUserMutation.error.message)
    }
    // result.data?.addUser?.id
    redirect('/users')
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2 className="font-bold text-5xl mb-8">Create new user</h2>
      <div className="flex flex-col gap-4 items-start">
        <label htmlFor="firstName" aria-required>
          FirstName:
        </label>
        <input
          id="firstName"
          type="text"
          name="firstName"
          required
          ref={firstNameRef}
          className="border border-slate-300 rounded-3xl py-2 px-4 w-full"
        />

        <label htmlFor="age" aria-required>
          Age:{' '}
        </label>
        <input
          id="age"
          type="number"
          name="age"
          required
          ref={ageRef}
          className="border border-slate-300 rounded-3xl py-2 px-4 w-full"
        />

        <label htmlFor="companyId">Company: </label>
        <Dropdown list={companies.map(c => ({ key: c.id, label: c.name }))} onSelect={item => setCompanyId(item.key)} />

        <button
          type="submit"
          disabled={addUserMutation.loading}
          className="w-full my-4 text-blue-500 bg-slate-200 rounded-2xl py-1 text-2xl">
          CREATE
        </button>
        {addUserMutation.error?.message && (
          <div className="text-red-500 font-medium text-center w-full">{addUserMutation.error.message}</div>
        )}
      </div>
    </form>
  )
}

export default User
