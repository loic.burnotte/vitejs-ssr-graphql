import Dropdown from './ui/dropdown'
import { useCompanyStore } from '../store/company'
import { FormEvent, useRef, useState } from 'react'
import type { UserType } from '../models/user.model'
import { useParams, useNavigate } from 'react-router-dom'
import { useQuery, useMutation } from '@apollo/client/index.js'
import { GET_USER, UPDATE_USER, DELETE_USER, GET_USERS } from '../GraphQL/user'

function User() {
  const redirect = useNavigate()
  const { companies } = useCompanyStore()

  const { id } = useParams()
  const { loading, error, data } = useQuery<{ user: UserType }>(GET_USER, {
    variables: { id },
  })

  const [companyId, setCompanyId] = useState(data?.user?.company?.id)
  const firstNameRef = useRef<HTMLInputElement>(null)
  const ageRef = useRef<HTMLInputElement>(null)

  const [editUser, editUserMutation] = useMutation(UPDATE_USER)
  const [deleteUser, deleteUserMutation] = useMutation(DELETE_USER)

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const firstName = firstNameRef.current?.value
    const age = Number(ageRef.current?.value)

    await editUser({
      variables: {
        id,
        firstName,
        age,
        companyId,
      },
    })
    if (editUserMutation.error) {
      console.error(editUserMutation.error.message)
      throw new Error(editUserMutation.error.message)
    }
    redirect('/users')
  }

  const handleDelete = async () => {
    await deleteUser({ variables: { id }, refetchQueries: [GET_USERS] })
    redirect('/users')
  }

  return (
    <>
      {loading && 'Loading user...'}
      {error && `Error: ${error.message}`}
      {data?.user ? (
        <form onSubmit={handleSubmit}>
          <h2 className="font-bold text-5xl mb-8">User: </h2>[{data.user?.id}]
          <div className="flex flex-col gap-4 items-start">
            <label htmlFor="firstName" aria-required>
              FirstName:
            </label>
            <input
              id="firstName"
              type="text"
              name="firstName"
              required
              ref={firstNameRef}
              defaultValue={data.user.firstName}
              className="border border-slate-300 rounded-3xl py-2 px-4 w-full"
            />

            <label htmlFor="age" aria-required>
              Age:
            </label>
            <input
              id="age"
              type="number"
              required
              name="age"
              ref={ageRef}
              defaultValue={data.user.age}
              className="border border-slate-300 rounded-3xl py-2 px-4 w-full"
            />

            <label htmlFor="companyId">Company: </label>
            <Dropdown
              list={companies.map(c => ({ key: c.id, label: c.name }))}
              item={data.user.company ? { key: data.user.company.id, label: data.user.company.name } : undefined}
              onSelect={item => setCompanyId(item.key)}
            />

            <button
              type="submit"
              disabled={editUserMutation.loading}
              className="w-full my-4 text-blue-500 bg-slate-200 rounded-2xl py-1 text-2xl">
              {editUserMutation.loading ? 'Loading...' : 'SAVE'}
            </button>
            {editUserMutation.error?.message && (
              <div className="text-red-500 font-medium text-center w-full">{editUserMutation.error.message}</div>
            )}
          </div>
          <div>
            <button type="button" className="text-red-500" onClick={handleDelete} disabled={deleteUserMutation.loading}>
              Delete user
            </button>
          </div>
        </form>
      ) : (
        <h4>No user!</h4>
      )}
    </>
  )
}

export default User
