import { useState } from 'react'

interface DropdownProps<T> {
  list: T[]
  item?: T
  onSelect: (item: T) => void
}

function Dropdown<T extends { key: string | number; label: string }>({ list, item, onSelect }: DropdownProps<T>) {
  const [selectedItem, setSelectedItem] = useState<T | undefined>(item)
  const [open, setOpen] = useState(false)

  const handleSelect = (item: T) => {
    setSelectedItem(item)
    setOpen(false)
    onSelect(item)
  }

  return (
    <>
      <button
        id="dropdownDefaultButton"
        data-dropdown-toggle="dropdown"
        onClick={() => setOpen(prev => !prev)}
        className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        type="button">
        {selectedItem ? selectedItem.label : 'Select Company'}
        <svg
          className="w-2.5 h-2.5 ms-3"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 10 6">
          <path stroke="currentColor" strokeLinecap="round" strokeWidth="2" d="m1 1 4 4 4-4" />
        </svg>
      </button>

      {/* <!-- Dropdown menu --> */}
      <div
        id="dropdown"
        className={`z-10 ${open ? 'block' : 'hidden'} bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700`}>
        <ul className="py-2 text-sm text-gray-700 dark:text-gray-200" aria-labelledby="dropdownDefaultButton">
          {list.map(_item => (
            <li
              key={_item.key}
              onClick={() => handleSelect(_item)}
              className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">
              {_item.label}
            </li>
          ))}
        </ul>
      </div>
    </>
  )
}

export default Dropdown
