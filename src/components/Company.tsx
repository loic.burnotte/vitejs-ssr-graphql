import { FormEvent, useRef } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import type { CompanyType } from '../models/company.model'
import { useMutation, useQuery } from '@apollo/client/index.js'
import { DELETE_COMPANY, GET_COMPANIES, GET_COMPANY, UPDATE_COMPANY } from '../GraphQL/company'

function Company() {
  const redirect = useNavigate()

  const nameRef = useRef<HTMLInputElement>(null)
  const descriptionRef = useRef<HTMLInputElement>(null)

  const { id } = useParams()

  const [editCompany, editCompanyMutation] = useMutation(UPDATE_COMPANY)
  const [deleteCompany, deleteCompanyMutation] = useMutation(DELETE_COMPANY)
  const { loading, error, data } = useQuery<{ company: CompanyType }>(GET_COMPANY, {
    variables: { id },
  })

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const name = nameRef.current?.value
    const description = descriptionRef.current?.value

    await editCompany({
      variables: {
        id,
        name,
        description,
      },
    })

    if (editCompanyMutation.error) {
      console.error(editCompanyMutation.error.message)
      throw new Error(editCompanyMutation.error.message)
    }
    redirect('/companies')
  }

  const handleDelete = async () => {
    await deleteCompany({ variables: { id }, refetchQueries: [GET_COMPANIES] })
    redirect('/companies')
  }

  return (
    <>
      {loading && 'Loading company...'}
      {error && `Error: ${error.message}`}
      {data?.company ? (
        <form onSubmit={handleSubmit}>
          <h2 className="font-bold text-5xl mb-8">Company ({data.company?.id})</h2>

          <div className="flex flex-col gap-4 items-start">
            <label htmlFor="name" aria-required>
              Name:
            </label>
            <input
              id="name"
              type="text"
              required
              name="name"
              ref={nameRef}
              defaultValue={data.company?.name}
              className="border border-slate-300 rounded-3xl py-2 px-4 w-full"
            />

            <label htmlFor="description">Description: </label>
            <input
              id="description"
              type="text"
              name="description"
              ref={descriptionRef}
              defaultValue={data.company?.description}
              className="border border-slate-300 rounded-3xl py-2 px-4 w-full"
            />

            <button
              type="submit"
              disabled={editCompanyMutation.loading}
              className="w-full my-4 text-blue-500 bg-slate-200 rounded-2xl py-1 text-2xl">
              SAVE
            </button>
            {editCompanyMutation.error?.message && (
              <div className="text-red-500 font-medium text-center w-full">{editCompanyMutation.error.message}</div>
            )}
          </div>

          <div>
            <button
              type="button"
              className="text-red-500"
              onClick={handleDelete}
              disabled={deleteCompanyMutation.loading}>
              Delete company
            </button>
          </div>
        </form>
      ) : (
        <h4>No Company!</h4>
      )}
    </>
  )
}

export default Company
