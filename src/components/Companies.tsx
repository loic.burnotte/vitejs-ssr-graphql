import { Link } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client/index.js'
import type { CompanyType } from '../models/company.model'
import { useCompanyStore } from '../store/company'
import { useEffect } from 'react'

function Companies() {
  const { setCompanies } = useCompanyStore()
  const GET_COMPANIES = gql`
    query GetCompanies {
      companies {
        id
        name
        description
      }
    }
  `
  const { loading, error, data } = useQuery<{ companies: CompanyType[] }>(GET_COMPANIES)

  useEffect(() => {
    if (data?.companies?.length) {
      setCompanies(data.companies)
    }
  }, [data])

  return (
    <div>
      <h1 className="text-4xl font-bold mb-4">Companies</h1>
      {loading && 'Loading companies...'}
      {error && `Error: ${error.message}`}
      {data ? (
        <ul>
          {data.companies?.map((company: CompanyType) => (
            <Link key={company.id} to={`/company/${company.id}`}>
              <li>
                {company.name}: {company.description}
              </li>
            </Link>
          ))}
          <Link key="add" to="/companies/add">
            <li className="rounded-3xl bg-cyan-500 text-white w-40 m-auto py-1 my-1">ADD COMPANY</li>
          </Link>
        </ul>
      ) : (
        <h4>No data!</h4>
      )}
    </div>
  )
}

export default Companies
