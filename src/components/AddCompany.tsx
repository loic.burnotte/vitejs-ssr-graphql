import { FormEvent, useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import { useMutation } from '@apollo/client/index.js'
import { ADD_COMPANY, GET_COMPANIES } from '../GraphQL/company'

function AddCompany() {
  const redirect = useNavigate()
  const nameRef = useRef<HTMLInputElement>(null)
  const descriptionRef = useRef<HTMLInputElement>(null)

  const [addCompany, addCompanyMutation] = useMutation(ADD_COMPANY)

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const name = nameRef.current?.value
    const description = descriptionRef.current?.value

    await addCompany({
      variables: {
        name,
        description,
      },
      refetchQueries: [GET_COMPANIES],
    })
    if (addCompanyMutation.error) {
      console.error(addCompanyMutation.error.message)
      throw new Error(addCompanyMutation.error.message)
    }
    // result.data?.addCompany?.id
    redirect('/companies')
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2 className="font-bold text-3xl mb-8">Create a new company</h2>
      <div className="flex flex-col gap-4 items-start">
        <label htmlFor="name" aria-required>
          Name:
        </label>
        <input
          id="name"
          type="text"
          name="name"
          required
          ref={nameRef}
          className="border border-slate-300 rounded-3xl py-2 px-4 w-full"
        />

        <label htmlFor="description">Description: </label>
        <input
          id="description"
          type="text"
          name="description"
          ref={descriptionRef}
          className="border border-slate-300 rounded-3xl py-2 px-4 w-full"
        />

        <button
          type="submit"
          disabled={addCompanyMutation.loading}
          className="w-full my-4 text-blue-500 bg-slate-200 rounded-2xl py-1 text-2xl">
          CREATE
        </button>
        {addCompanyMutation.error?.message && (
          <div className="text-red-500 font-medium text-center w-full">{addCompanyMutation.error.message}</div>
        )}
      </div>
    </form>
  )
}

export default AddCompany
