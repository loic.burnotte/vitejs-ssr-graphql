import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client/index.js'

import Router from './router'

const apolloClientURI = import.meta.env.VITE_APOLLO_CLIENT_URI

const client = new ApolloClient({
  uri: apolloClientURI,
  cache: new InMemoryCache(),
})

function Main() {
  return (
    <ApolloProvider client={client}>
      <Router />
    </ApolloProvider>
  )
}

export default Main
