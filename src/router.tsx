import { Routes, Route, Link } from 'react-router-dom'
import { PropsWithChildren, Suspense, lazy } from 'react'

import App from './App'
import React from 'react'
const Company = lazy(() => import('./components/Company'))
const Companies = lazy(() => import('./components/Companies'))
const AddCompany = lazy(() => import('./components/AddCompany'))

const User = lazy(() => import('./components/User'))
const Users = lazy(() => import('./components/Users'))
const AddUser = lazy(() => import('./components/AddUser'))

const fallback = <div>Loading...</div>

function Header({ children }: PropsWithChildren) {
  const [Component, setComponent] = React.useState<React.ReactNode>(() => fallback)

  React.useEffect(() => {
    setComponent(() => children)
  }, [children])

  return (
    <div className="w-full h-full">
      <div className="h-20 w-full flex flex-row items-center justify-between bg-slate-100 px-12 mb-8">
        <Link to="/">Home</Link>
        <div className="flex gap-10">
          <Link to="/companies">Companies</Link>
          <Link to="/users">Users</Link>
        </div>
      </div>
      <div className="max-w-4xl m-auto">
        <Suspense fallback={fallback}>{Component}</Suspense>
      </div>
    </div>
  )
}

function Router() {
  const ErrorPage = <div>Not found!</div>

  return (
    <Routes>
      <Route
        path="/"
        element={
          <Header>
            <App />
          </Header>
        }
        errorElement={ErrorPage}
      />
      <Route
        path="/users"
        element={
          <Header>
            <Users />
          </Header>
        }
      />
      <Route
        path="/user/:id"
        element={
          <Header>
            <User />
          </Header>
        }
      />
      <Route
        path="/users/add"
        element={
          <Header>
            <AddUser />
          </Header>
        }
      />

      <Route
        path="/companies"
        element={
          <Header>
            <Companies />
          </Header>
        }
      />
      <Route
        path="/company/:id"
        element={
          <Header>
            <Company />
          </Header>
        }
      />
      <Route
        path="/companies/add"
        element={
          <Header>
            <AddCompany />
          </Header>
        }
      />
    </Routes>
  )
}

export default Router
