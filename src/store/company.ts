import { create } from 'zustand'
import { CompanyType } from '../models/company.model'

type CompanyStore = {
  companies: CompanyType[]
  setCompanies: (companies: CompanyType[]) => void
}

export const useCompanyStore = create<CompanyStore>(set => ({
  companies: [],
  setCompanies: companies => set({ companies }),
}))
