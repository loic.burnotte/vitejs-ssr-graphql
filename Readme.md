## Installation

```js
cd vitejs-ssr-graphql
npm i
npm run start
```

## Start project

1. JSON-SERVER (http://localhost:3000)

```js
npm run server
```

2. EXPRESS, ViteJS (http://localhost:5173), GraphQL (/graphql) & Ruru (/api)

```js
npm run dev
```

## Demo

### Ruru `http://localhost:5173/api`

![alt text](./src/assets/image.png)

### GraphQL `http://localhost:5173/graphql?query={%20user(id:%20"23")%20{%20id,%20firstName,%20age%20}%20}`

```json
{
  "data": {
    "user": {
      "id": "23",
      "firstName": "Phill",
      "age": 20
    }
  }
}
```

### Frontend `http://localhost:5173`

![alt text](./src/assets/image-2.png)
![alt text](./src/assets/image-3.png)
![alt text](./src/assets/image-4.png)

## Packages

### Server

- Express => `http://localhost:5173` (the same for the Frontend because of the SSR)
- SSR viteJS
- GraphQL => `http://localhost:5173/graphql`
- Ruru (to replace <i>GraphiQL</i> since the <strong>[express-graphql](https://www.npmjs.com/package/express-graphql)</strong> package has been [deprecated](https://graphql.org/graphql-js/running-an-express-graphql-server/) and replaced by <strong>[graphql-http](https://www.npmjs.com/package/graphql-http)</strong>) => `http://localhost:5173/api`

### Frontend

- [VitesJS (SSR)](https://vitejs.dev/guide/ssr)
- [Tailwind CSS](https://tailwindcss.com/)
- [@apollo/client](https://www.apollographql.com/docs/react/)
- [Zustand](https://zustand-demo.pmnd.rs/)
- [React Router](https://reactrouter.com/en/main)

## Infrastructure

|- schema (for GraphQL)
|- server [BACKEND]
| |- models
| |- schema
| |- mongoose.js (not implemented yet - JSON server is running for now)
| | server.js
|- src [FRONTEND]
| |- assets
| |- components
| |- ui
| |- GraphQL (requests)
| |- models
| |- store (zustand for Companies - could be replaced with the cached graphql request ?!)
| | App.tsx (homepage)
| | main.tsx (contexts)
| | entry-client.tsx (index file for the frontend) SSR
| | entry-client.tsx (index file for the backend) SSR
| | router.tsx (routing)
| | index.css (main style)
